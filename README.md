# RPNX System Standards

This document describes standards for code quality.

# Section 1 - General Programming

## 1.1 - Do not encode URL or PATH constants in code

Do not include constants like URLs or PATHs in code. If you must include
paths or URLs in compiled code, they should be provided as build configuration.

## 1.2 - Do not use IP Addresses as Security

### Rule

Applications MUST NOT use IP addresses as a substituite for security such as
passwords, or public key encryption. Applications MAY use IP addresses for DoS
protection.

### Rationale

Applications should be tolerant of multi-tenant machines where other users on
the same device may not be trustworthy. Furthermore, being on the same LAN
should not cause compromise of the entire network. 


# Section 2 - C/C++

## 2.1 - Use snake_case for classes, namespaces, and functions

### Rule

C++ projects SHOULD use snake_case for classes, namespaces, and functions.

### Rationale

This decision is completely arbitrary, but if a standard is to be set,
this seems like the best option for consistency.

## 2.2 - Use unsigned integers for indexing

### Rule

Indexing SHOULD be done with unsigned integers of the correct size.

### Rationale

Signed integers are more error prone, as they require two, rather 
than one, comparisons. This makes coder slower and potentially introduces
bugs if someone forgets to test for `i >= 0`.

### Tips

Instead of using -1 as a sentinel value, use `std::optional<std::size_t>`.

## 2.3 - Use appropriately sized values for indexing.

### Rule

Programs MUST use appropriately sized values for indexing.

### Rationale

Usage of "int" or other wrongly sized types can cause the program to break
when processing large amounts of data.

### Tips

`std::size_t` is usually the correct type for indexing.

## 2.4 - Do not serialize/deserialize using memcpy or similar

### Rule

Programs MUST NOT serailize or deserialize objects using memcpy or similar.

### Rationale

Memcpy is more error prone. There are situations where binary layouts of
structs differ between platforms or compilers, and the layout should be 
captured in code.

Using bitwise shifts to save/load integers may seem less efficient, but 
actually isn't when compiler optimizations are enabled. And it guarantees 
correct behavior regardless of layout or endian.

### Exception

On some platforms, some structs returned by the kernel may be defined to
be in C compiler layout. Ideally one would still define these structs
using code, but that may be too much work in practice.


## 2.5 - Prefer CMake

### Rule

Pure C/C++ projects SHOULD use CMake as the build system.

### Rationale

CMake is the most widely supported build system for C++.


## 2.6 - Check Correctly

### Rule

When testing for a compiler feature, applications MUST use a compiler test. 
When testing for a platform, applications MUST use a platform test.
When testing for a standard library, applications MUST use a library test.

### Explanation

A common and frequent mistake is to check for a specific compiler or platform
before using a particular standard library features, or another combination.

### Noncompliant Example

```
// Within an x86 context...
#ifdef _WIN32
__asm {
    mov eax, 1
    add eax, 2
}
#else
__asm__("movl $1, %eax\n\t"
        "addl $2, %eax");
#endif
```

This code uses the Microsoft compiler assembly syntax on Windows and uses the GCC
syntax on other platforms. This is wrong because it is possible to use other
compilers on Windows and some compilers that support Linux do not accept the GCC
syntax.

### Compliant Example

```
#if defined(_MSC_VER)
// Microsoft Visual Studio compiler (Intel syntax)
__asm {
    mov eax, 1
    add eax, 2
}
#elif defined(__GNUC__) || defined(__clang__)
// GCC or Clang compiler (AT&T syntax)
__asm__("movl $1, %eax\n\t"
        "addl $2, %eax");
#else
 ...
#endif
```

This example correctly tests for the compiler instead of the operating system when
invoking compiler-specific extensions.


# Section 3 - CMake


## 3.1 - Do not modify compiler flags in CMakeLists

### Rule

CMakeLists, and files included therefrom, MUST NOT modify compiler flags.

### Rationale

Following the principle of least surprise, all modifications to compiler 
flags should be done in a toolchain file or on the command line to CMake.

Modifying command lines tends to break projects when non-standard
compilers are used.

One example where this is relevant is certain projects choosing to disable
RTTI or exception handling. This behavior can break code compatibility.

If a particular set of "default" compiler flags is suggested by a project,
they must be included in a build script or toolchain file.

A user of a library should be able to extract the library, and compile
it using a single toolchain file into a larger project that uses
consistent compiler flags throughout the codebase.


## 3.2 - Do not encode absolute paths in exports

### RULE
Exports MUST NOT include absolute paths (e.g. `/usr/lib/boost`).

### Rationale
An export should be able to be extracted to an arbitrary path and function
correctly. e.g. it should be possible to unpack at arbitrary locations,
such as `/home/buildbot/workspace_ac87fb2811/deps/library_3.29.9`,
and have the export function correctly.

## 3.3 - Do not use CMake as a dependency manager

### Rule

CMake MUST NOT be used as a dependency manager. CMake MUST NOT access the
internet.

### Rationale

Using CMake as a dependency manager leads to surprising behaviors that users
do not expect, as well as non-reproducible builds.

## 3.4 - Export libraries

### Rule

Libraries SHOULD be exported via both export and install export.

### Rationale

Projects that export via both methods can be used via find_package
using targets on both installed systems and build trees.

# Section 4 - GCC/Clang

## 4.1 - Do not use -fvisibility-inlines=hidden

### Rule

Compiler flags MUST NOT contain -fvisibility-inlines=hidden. 

### Rationale

This flag causes certain valid standards-compliant C++ code to behave 
incorrectly and break.

While this flag can improve build times, unfortunately it does so
by playing fast and loose with the C++ rules in a manner that can
cause multiple static objects to be created for inline functions,
despite the C++ standard mandating only 1 such object exist per
declaration in the compiled program. Code that depends on this
standard behavior may break.

### Exception

Projects which audit all uses of inline functions, including all
uses in libraries imported as dependencies, for instances of static 
object within inline functions (including functions not marked as
inline, but treated as inline, such as certain templates and functions
declared at class scope).

# Section 5 - MSVC

## 5.1 - Declare NOMINMAX

### Rule

Toolchain files used on Windows SHOULD declare the NOMINMAX macro by default.

### Rationale

Windows headers by default break the C++ standard library by setting macros
for "min" and "max". The default behavior should be a working C++ standard
library.

### Exception

Legacy code for Windows-only projects.
